#include <Arduino.h>
#include "WiFi.h"
//#include "WiFiClient.h"
//#include "WiFiAP.h"
#include "ESPAsyncWebServer.h"
#include "SPIFFS.h"
//#include <HardwareSerial.h>

const char* ssid = "9S_hui";
const char* password = "qazwsx312";

const int ledPin = 13;

AsyncWebServer server(80);

struct bt_scan_data 
{
  std::vector<String> _bt_d_name;
  std::vector<String> _bt_d_address;
  std::vector<uint8_t> _d_name_size;
  uint8_t _count;
};

String processor(const String& var){
  String ledState;

  Serial.println(var);
  if(var == "STATE"){
    if(digitalRead(ledPin)){
      ledState = "ON";
    }
    else{
      ledState = "OFF";
    }
    Serial.print(ledState);
    return ledState;
  }
  return String();
}  

String ass(const String& var){
  bt_scan_data bt_d_data;
  int siz = 0;
  if (Serial2.available())
  {
    String bt_d_n = "";
    String bt_d_a = "";
    std::vector<int> displs;
    int sum = 0;
    displs.push_back(sum);
    while (Serial2.available()>0)
      {
        bt_d_data._count = (char)Serial2.read();
        siz = bt_d_data._count-48;
        for (int i = 0; i < siz; i++)
        { 
          bt_d_data._d_name_size.push_back((char)Serial2.read());
          displs.push_back(sum += bt_d_data._d_name_size[i]-48);
        }

        for (int i = 0; i < siz; i++)
        { 
          
          for (int j = displs[i]; j < displs[i+1]; j++)
            bt_d_n += (char)Serial2.read();
          bt_d_data._bt_d_name.push_back(bt_d_n);
          bt_d_n = "";
        }

        for (int i = 0; i < siz; i++)
        {
          for (int j = 17*i; j < 17*(i+1); j++)
            bt_d_a += (char)Serial2.read();
          bt_d_data._bt_d_address.push_back(bt_d_a);
          bt_d_a = "";

        }
      }
  }
  for (int i = siz; i < 3; i++)
  {
    bt_d_data._bt_d_name.push_back("None");
    bt_d_data._bt_d_address.push_back("None");
  }
  String text_state = "";

  Serial.println("<===============>");
    for (int i = 0; i < siz; i++)
    {
      Serial.print("Name: ");
      Serial.println(bt_d_data._bt_d_name[i].c_str());
      Serial.print("Address: ");
      Serial.println(bt_d_data._bt_d_address[i].c_str());
      text_state = "Name: ";
      text_state.concat(bt_d_data._bt_d_name[i] + "Address: "+ bt_d_data._bt_d_address[i] + "; ");
    }
    Serial.println("<===============>");
  String tt = "";
  for (int i = 0; i < siz; i++)
  {
     text_state.concat("Name: "+ bt_d_data._bt_d_name[i] + "Address: "+ bt_d_data._bt_d_address[i] + "; ");
  }
  if(var == "STATE")
  { 
  if (true){
     text_state.concat("Name: "+ bt_d_data._bt_d_name[siz-1] + "Address: "+ bt_d_data._bt_d_address[siz-1] + "; ");
 
    //text_state = "zaebis";
  }
  else{
    text_state = "huy tebe";
  }
  }
  Serial.println(text_state);
  text_state.clear();
  Serial.println(text_state);

      for (int i = 0; i < siz; i++)
  {
     text_state.concat("Name: "+ bt_d_data._bt_d_name[i] + "Address: "+ bt_d_data._bt_d_address[i] + "; ");
  }
  return text_state;
  //return String();
} 
 
void setup(){
  Serial2.begin(115200);
  Serial.begin(115200);
  pinMode(ledPin, OUTPUT);

  // Инициализируем SPIFFS:
  if(!SPIFFS.begin(true)){
    Serial.println("An Error has occurred while mounting SPIFFS");
    return;
  }

  WiFi.softAP(ssid, password);
  Serial.print("IP: ");
  IPAddress myIP = WiFi.softAPIP();
  Serial.println(myIP);

  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/index.html", String(), false, processor);
  });
  
  server.on("/style.css", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/style.css", "text/css");
  });

  server.on("/on", HTTP_GET, [](AsyncWebServerRequest *request){
    //digitalWrite(ledPin, HIGH);    
    request->send(SPIFFS, "/index.html", String(), false, processor);
  });
  
  server.on("/off", HTTP_GET, [](AsyncWebServerRequest *request){
    //digitalWrite(ledPin, LOW);    
    request->send(SPIFFS, "/index.html", String(), false, processor);
  });

  server.on("/update", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(SPIFFS, "/index.html", String(), false, ass);
  });

  server.begin();
  Serial.println("Сервер запущен.");
}
 
void loop(){

  // Serial.println("<===============>");
  // for (int i = 0; i < siz; i++)
  // {
  //   Serial.print("Name: ");
  //   Serial.println(bt_d_data._bt_d_name[i].c_str());
  //   Serial.print("Address: ");
  //   Serial.println(bt_d_data._bt_d_address[i].c_str());
  // }
  // Serial.println("<===============>");


  delay(2000);
  IPAddress myIP = WiFi.softAPIP();
  Serial.println(myIP);
  Serial.println();


  
}